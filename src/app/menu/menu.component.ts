import { Component, OnInit, Inject } from '@angular/core';

import { DishService } from '../services/dish.service';
import { Dish } from '../shared/dish';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  dishes: Dish[] ;
  errMess: string;

  constructor(private dishService: DishService,
              @Inject('baseURL') public baseURL) { }

  ngOnInit(): void {
    /* Con Promises
    this.dishService.getDishes()
      .then(dishes => this.dishes = dishes);
     */
    this.dishService.getDishes()
      .subscribe(dishes => this.dishes = dishes,
        errmess => this.errMess = <any> errmess);
  }
}
