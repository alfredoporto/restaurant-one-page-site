export class Comment {
  name: string;
  value: number;
  comment: string;
  date: string;
}
